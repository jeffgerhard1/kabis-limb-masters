# Non-Scribe book scanning workflow
## Kabis, LIMB, and archival masters work 
## Can also include other scanners and software like the Zeutschel

This README file contains an overview of the contents of this repository. The links to individual notebooks will only work in a Jupyter environment. See the [setup](setup.md) file for more information.

***

## Introduction

This is code intended to manage the flow of files and data for non-Scribe book scanning. This work is tracked in a [spreadsheet](https://docs.google.com/spreadsheets/d/1b6L9IAGpA5rWBCcfkgpMcsn1jS6DpBL6i0XOsbH_RI4/edit#gid=0)  and important data is copied into the digitization database.

Similar to managing microfilm, the goal here is to utilize Google Drive as a holdings directory.

The purpose of these notebooks is to:

1. Facilitate file transfers
    - Kabis scans (folders of .cr2 files) to LIMB computer
    - Post-LIMB, same files to masters computer
    - PDFs to another holdings area (Y Drive or Google Drive?) for IA upload
    - PDFs to Internet Archive
    - Possibly archival bags to storage drive
2. Update the database with information pulled from the spreadsheets including:
    - Kabis scan date
    - Image counts
    - PDF created
    - IA upload
        - this requires grabbing metadata (MARC record, volume info.) as well
    - Archival masters created
    - Files bagged
3. Add some extra data to the GSheet and also manage updates to it?

***



## General outline

