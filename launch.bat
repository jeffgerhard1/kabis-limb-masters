@echo off
goto :TopOfCode

====================================================
2019 batch script to launch the newer versions of
the Jupyter notebooks

====================================================



:TopOfCode
@echo on
if exist %LOCALAPPDATA%\Continuum\anaconda3\ set root=%LOCALAPPDATA%\Continuum\anaconda3
if exist %LOCALAPPDATA%\Continuum\miniconda3\ set root=%LOCALAPPDATA%\Continuum\miniconda3
if exist %UserProfile%\Miniconda3\ set root=%UserProfile%\Miniconda3
call %root%\Scripts\activate.bat %root%
call jupyter lab